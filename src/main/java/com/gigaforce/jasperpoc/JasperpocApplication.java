package com.gigaforce.jasperpoc;

import com.gigaforce.jasperpoc.entity.Letter;
import com.gigaforce.jasperpoc.entity.LetterContent;
import com.gigaforce.jasperpoc.entity.LetterContentAddress;
import com.gigaforce.jasperpoc.entity.LetterType;
import com.gigaforce.jasperpoc.repository.LetterRepository;
import com.github.javafaker.Faker;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class JasperpocApplication implements CommandLineRunner {

	String msg = "State Auto has paid their insured Donald Parsley for damages sustained in an accident on 07/29/2021. National" +
			"Subrogation Services, as recovery agent for State Auto has investigated the facts and determined the accident" +
			"was due to your insured’s negligence.\n" +
			"Due to the payment to Donald Parsley, we are pursuing the subrogation rights of State Auto. The amount we" +
			"are claiming is the amount paid plus the deductible as noted above. We have enclosed documentation to" +
			"substantiate our claim." ;

	@Autowired
	private LetterRepository letterRepository;

	Faker faker = new Faker();

	public static void main(String[] args) {
		SpringApplication.run(JasperpocApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// Cleanup the users table
		letterRepository.deleteAllInBatch();

		// Insert a new user in the database

		LetterContentAddress
				letterContentAddress = new LetterContentAddress(faker.address().fullAddress(), faker.address().secondaryAddress(),
																faker.phoneNumber().phoneNumber(), faker.phoneNumber().extension());

		LetterContentAddress
				letterContentAddress1 = new LetterContentAddress(faker.address().fullAddress(), faker.address().secondaryAddress(),
																 faker.phoneNumber().phoneNumber(), faker.phoneNumber().extension());

		LetterContentAddress
				letterContentAddress2 = new LetterContentAddress(faker.address().fullAddress(), faker.address().secondaryAddress(),
																 faker.phoneNumber().phoneNumber(), faker.phoneNumber().extension());

		List<LetterContentAddress> letterContentAddressList = new ArrayList<>();
		letterContentAddressList.add(letterContentAddress);
		letterContentAddressList.add(letterContentAddress1);
		letterContentAddressList.add(letterContentAddress2);

		LetterContent letterContent = new LetterContent(faker.superhero().name(), "07/22/2023",
														faker.number().digits(10), faker.number().digits(15), faker.company().name(),
														faker.address().cityName(),faker.number().digits(5),faker.number().digits(5),
														faker.number().digits(3),faker.superhero().name(),faker.number().digits(7),
														faker.number().digits(5),faker.number().digits(7),msg,
														faker.name().fullName(), faker.phoneNumber().phoneNumber(),
														faker.phoneNumber().extension(), faker.internet().emailAddress(),
														letterContentAddressList);

		Letter letter = new Letter();

		letter.setCompanyName("Gigaforce");
		letter.setLetterType(LetterType.DEMAND);
		letter.setLetterContent(letterContent);

		letterRepository.save(letter);

		// Insert a new user in the database

		LetterContentAddress
				letterContentAddress3 = new LetterContentAddress(faker.address().fullAddress(), faker.address().secondaryAddress(),
																 faker.phoneNumber().phoneNumber(), faker.phoneNumber().extension());


		List<LetterContentAddress> letterContentAddressList1 = new ArrayList<>();
		letterContentAddressList1.add(letterContentAddress3);

		LetterContent letterContent1 = new LetterContent(faker.superhero().name(), "07/30/2023",
														 faker.number().digits(10), faker.number().digits(15), faker.company().name(),
														 faker.address().cityName(),faker.number().digits(5),faker.number().digits(5),
														 faker.number().digits(3),faker.superhero().name(),faker.number().digits(7),
														 faker.number().digits(5),faker.number().digits(7),msg,
														 faker.name().fullName(), faker.phoneNumber().phoneNumber(),
														 faker.phoneNumber().extension(), faker.internet().emailAddress(),
														 letterContentAddressList1);

		Letter letter1 = new Letter();

		letter1.setCompanyName("National Subrogation Service");
		letter1.setLetterType(LetterType.ACKNOWLEDGEMENT);
		letter1.setLetterContent(letterContent1);

		letterRepository.save(letter1);
	}

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}
}
