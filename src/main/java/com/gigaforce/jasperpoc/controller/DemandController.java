package com.gigaforce.jasperpoc.controller;

import com.gigaforce.jasperpoc.entity.Letter;
import com.gigaforce.jasperpoc.model.Demand;
import com.gigaforce.jasperpoc.service.DemandService;
import net.sf.jasperreports.engine.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

@RestController
public class DemandController {

    final Logger log = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private DemandService demandService;

    private final String template_path = "/demand.jrxml";

    private static final String logo_path = "/logo.png";

    @GetMapping("/demand")
    public ResponseEntity<Demand> getDemand(){
        return new ResponseEntity<>(demandService.getDemand(), HttpStatus.OK);
    }

    @GetMapping("/letter/{id}")
    public ResponseEntity<Letter> getLetter(@PathVariable Long id){
        return new ResponseEntity<>(demandService.getLetter(id), HttpStatus.OK);
    }

    @GetMapping("/demand/print")
    public ResponseEntity<byte[]> getDemandprint() {

        log.info("Inside demand method.");

        try {

           Demand demand = demandService.getDemand();

            // Adding the additional parameters to the pdf.
            final Map<String, Object> parameters = new HashMap<>();
            parameters.put("demand", demand);
            parameters.put("company", demand.getCompanyName());
            parameters.put("logo", getClass().getResourceAsStream(logo_path));

            return jasperhandler(parameters);

        } catch (JRException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/email/print")
    public ResponseEntity<byte[]> getEmailprint() {

        log.info("Inside email method.");

        try {

            Demand demand = demandService.getEmail();

            // Adding the additional parameters to the pdf.
            final Map<String, Object> parameters = new HashMap<>();
            parameters.put("demand", demand);
            parameters.put("company", demand.getCompanyName());
            parameters.put("logo", getClass().getResourceAsStream(logo_path));

            return jasperhandler(parameters);

        } catch (JRException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private ResponseEntity<byte[]> jasperhandler (Map<String, Object> parameters) throws JRException {
        // Fetching the .jrxml file from the resource folder.
        final InputStream stream = this.getClass().getResourceAsStream(template_path);

        // Compile the Jasper report from .jrxml to .japser
        final JasperReport report = JasperCompileManager.compileReport(stream);

        // Fetching the employees from the data source.
        //JRBeanCollectionDataSource signDataBean = new JRBeanCollectionDataSource(employeeService.getDemand());

        // Filling the report with the employee data and additional parameters information.
        final JasperPrint print = JasperFillManager.fillReport(report, parameters, new JREmptyDataSource());
        //new JREmptyDataSource()

        HttpHeaders headers = new HttpHeaders();
        //set the PDF format
        headers.setContentType(MediaType.APPLICATION_PDF);
        headers.setContentDispositionFormData("filename", "demand.pdf");
        //create the report in PDF format
        return new ResponseEntity<>(JasperExportManager.exportReportToPdf(print), headers, HttpStatus.OK);
    }
}
