package com.gigaforce.jasperpoc.controller;

import com.gigaforce.jasperpoc.model.Subject;
import com.gigaforce.jasperpoc.service.EmployeeService;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

@RestController
public class EmailController {

    final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    EmployeeService employeeService;

    private static final String logo_path = "/logo.png";
    private final String template_path = "/email.jrxml";
    private static final String company = "Gigaforce";

    @GetMapping("/email")
    public ResponseEntity<byte[]> getEmail() {

        log.info("Inside email method.");

        try {
            // Fetching the .jrxml file from the resource folder.
            final InputStream stream = this.getClass().getResourceAsStream(template_path);

            // Compile the Jasper report from .jrxml to .japser
            final JasperReport report = JasperCompileManager.compileReport(stream);

            // Fetching the employees from the data source.
            JRBeanCollectionDataSource source = new JRBeanCollectionDataSource(employeeService.getEmailBody());
            //JRBeanCollectionDataSource addressDataBean = new JRBeanCollectionDataSource(employeeService.findAddress(jsonNode));
            //JRBeanCollectionDataSource signDataBean = new JRBeanCollectionDataSource(employeeService.getSign());

            Subject subject1 = employeeService.getSubject();

            // Adding the additional parameters to the pdf.
            final Map<String, Object> parameters = new HashMap<>();
            parameters.put("logo", getClass().getResourceAsStream(logo_path));
            parameters.put("company", company);
            //parameters.put("addressDataset", addressDataBean );
            parameters.put("subject", subject1);
            //parameters.put("signDataset",signDataBean);


            // Filling the report with the employee data and additional parameters information.
            final JasperPrint print = JasperFillManager.fillReport(report, parameters, source);
            //new JREmptyDataSource()

            HttpHeaders headers = new HttpHeaders();
            //set the PDF format
            headers.setContentType(MediaType.APPLICATION_PDF);
            headers.setContentDispositionFormData("filename", "email.pdf");
            //create the report in PDF format
            return new ResponseEntity<>(JasperExportManager.exportReportToPdf(print), headers, HttpStatus.OK);

        } catch (JRException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
