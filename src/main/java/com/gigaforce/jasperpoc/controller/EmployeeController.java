package com.gigaforce.jasperpoc.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.gigaforce.jasperpoc.entity.MasterLetter;
import com.gigaforce.jasperpoc.model.dtos.MasterKeyDto;
import com.gigaforce.jasperpoc.model.dtos.MasterKeyListDto;
import com.gigaforce.jasperpoc.service.EmployeeService;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class EmployeeController {

    final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    EmployeeService employeeService;

    private static final String gigaforce_logo_path = "/logo.png";
    private static final String nss_logo_path = "/nss.png";

    @PostMapping("/employee/report")
    public ResponseEntity<byte[]> getEmployeeRecordReport(@RequestBody JsonNode jsonNode) {

        try {
            // Fetching the .jrxml file from the resource folder.
            final InputStream stream = this.getClass().getResourceAsStream("/tables_data.jrxml");

            // Compile the Jasper report from .jrxml to .japser
            final JasperReport report = JasperCompileManager.compileReport(stream);

            // Fetching the details from the data source.
            JRBeanCollectionDataSource addressDataBean = new JRBeanCollectionDataSource(employeeService.findAddress(jsonNode));
            JRBeanCollectionDataSource subjectBean = new JRBeanCollectionDataSource(employeeService.findAll(jsonNode));
            JRBeanCollectionDataSource bodyBean = new JRBeanCollectionDataSource(employeeService.getBodyFromJson(jsonNode));
            JRBeanCollectionDataSource signDataBean = new JRBeanCollectionDataSource(employeeService.getSign(jsonNode));
            JRBeanCollectionDataSource footerBean = new JRBeanCollectionDataSource(employeeService.findAddress(jsonNode));
            String companyName = employeeService.getCompanyName(jsonNode);

            // Adding the additional parameters to the pdf.
            final Map<String, Object> parameters = new HashMap<>();
            parameters.put("company", companyName);
            parameters.put("logo", getLogo(companyName));
            parameters.put("addressdataset", addressDataBean);
            parameters.put("subdataset",subjectBean);
            parameters.put("bodydataset", bodyBean);
            parameters.put("signDataset",signDataBean);
            parameters.put("footerdataset",footerBean);

            // Filling the report with the employee data and additional parameters information.
            final JasperPrint print = JasperFillManager.fillReport(report, parameters, new JREmptyDataSource());

            HttpHeaders headers = new HttpHeaders();
            //set the PDF format
            headers.setContentType(MediaType.APPLICATION_PDF);
            headers.setContentDispositionFormData("filename", "report.pdf");
            //create the report in PDF format
            return new ResponseEntity<>(JasperExportManager.exportReportToPdf(print), headers, HttpStatus.OK);

        } catch (JsonProcessingException | JRException e) {
            throw new RuntimeException(e);
        }
    }

    public InputStream getLogo(String companyName){
        InputStream resourceAsStream = null;
        if(companyName.equalsIgnoreCase("Gigaforce")){
            resourceAsStream = getClass().getResourceAsStream(gigaforce_logo_path);
        } else if (companyName.equalsIgnoreCase("National Subrogation Service")) {
            resourceAsStream = getClass().getResourceAsStream(nss_logo_path);
        }else {
            resourceAsStream = getClass().getResourceAsStream(gigaforce_logo_path);
        }
        return resourceAsStream;
    }

    @PostMapping("/save/masterData")
    public String saveMasterData(@RequestBody MasterKeyListDto masterKeyListDto){

        employeeService.saveMasterData(masterKeyListDto);

        return "Master Data saved.";
    }

    @PostMapping("/save/masterLetter")
    public String saveMasterLetter(@RequestBody MasterLetter masterLetter){

        employeeService.saveMasterLetter(masterLetter);

        return "Master Letter saved.";
    }
}
