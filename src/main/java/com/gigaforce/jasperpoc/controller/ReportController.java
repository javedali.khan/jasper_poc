package com.gigaforce.jasperpoc.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.gigaforce.jasperpoc.entity.MasterKey;
import com.gigaforce.jasperpoc.model.Demand;
import com.gigaforce.jasperpoc.model.dtos.RequestDto;
import com.gigaforce.jasperpoc.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ReportController {
    @Autowired
    private ReportService reportService;

    @GetMapping("/letter/print/{id}")
    public ResponseEntity<byte[]> getLetterPrint(@PathVariable Long id){
        return reportService.getReport(id);
    }

    @GetMapping("/letter/json/{id}")
    public ResponseEntity<Demand> getLetterJson(@PathVariable Long id) throws JsonProcessingException {
        return new ResponseEntity<>(reportService.getLetterContent(id), HttpStatus.OK);
    }

    @PostMapping("/letter/save")
    public ResponseEntity<RequestDto> saveLetter(@RequestBody RequestDto requestDto) throws JsonProcessingException {
        return new ResponseEntity<>(reportService.saveLetter(requestDto), HttpStatus.OK);
    }


    @PostMapping("/read")
    public ResponseEntity<List<MasterKey>> readJson(@RequestBody JsonNode jsonNode) throws JsonProcessingException {

        return  new ResponseEntity<>(reportService.readJson(jsonNode),HttpStatus.OK);
    }




}
