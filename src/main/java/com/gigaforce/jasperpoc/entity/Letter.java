package com.gigaforce.jasperpoc.entity;

import javax.persistence.*;
import java.util.UUID;

@Entity
public class Letter {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    private UUID claimUUID;
    private String companyName;
    @Enumerated(EnumType.STRING)
    @Column(name="letterType")
    private LetterType letterType;
    @OneToOne(cascade = CascadeType.ALL)
    private LetterContent letterContent;
    public Letter () {
    }

    public Letter (UUID claimUUID, String companyName, LetterType letterType, LetterContent letterContent) {
        this.claimUUID = claimUUID;
        this.companyName = companyName;
        this.letterType = letterType;
        this.letterContent = letterContent;
    }

    public Long getId () {
        return id;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public UUID getClaimUUID () {
        return claimUUID;
    }

    public void setClaimUUID (UUID claimUUID) {
        this.claimUUID = claimUUID;
    }

    public String getCompanyName () {
        return companyName;
    }

    public void setCompanyName (String companyName) {
        this.companyName = companyName;
    }

    public LetterType getLetterType () {
        return letterType;
    }

    public void setLetterType (LetterType letterType) {
        this.letterType = letterType;
    }

    public LetterContent getLetterContent () {
        return letterContent;
    }

    public void setLetterContent (LetterContent letterContent) {
        this.letterContent = letterContent;
    }
}
