package com.gigaforce.jasperpoc.entity;

import javax.persistence.*;

@Entity
public class LetterContentAddress {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstAddress;
    private String secondAddress;
    private String phone;
    private String fax;

/*    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, optional = false)
    @JoinColumn(name = "letter_content_id")
    private LetterContent letterContent;*/

    public LetterContentAddress () {
    }

    public LetterContentAddress (String firstAddress, String secondAddress, String phone, String fax) {
        this.firstAddress = firstAddress;
        this.secondAddress = secondAddress;
        this.phone = phone;
        this.fax = fax;
    }

    public Long getId () {
        return id;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public String getFirstAddress () {
        return firstAddress;
    }

    public void setFirstAddress (String firstAddress) {
        this.firstAddress = firstAddress;
    }

    public String getSecondAddress () {
        return secondAddress;
    }

    public void setSecondAddress (String secondAddress) {
        this.secondAddress = secondAddress;
    }

    public String getPhone () {
        return phone;
    }

    public void setPhone (String phone) {
        this.phone = phone;
    }

    public String getFax () {
        return fax;
    }

    public void setFax (String fax) {
        this.fax = fax;
    }


}

