package com.gigaforce.jasperpoc.entity;

import lombok.Getter;

@Getter
public enum LetterType {
    INFO("INFO"),
    CONTRIBUTION("INFO"),
    DEMAND("DEMAND"),
    OV("OV"),
    ACKNOWLEDGEMENT("ACKNOWLEDGEMENT"),
    CLOSING("CLOSING"),
    SETTLEMENT("SETTLEMENT");

    private String type;
    private LetterType(String type) {this.type = type;}
    public String getType() {return this.type;}
}
