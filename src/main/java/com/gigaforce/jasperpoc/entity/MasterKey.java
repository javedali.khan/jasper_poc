package com.gigaforce.jasperpoc.entity;

import javax.persistence.*;

@Entity
public class MasterKey {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique=true)
    private String titleKey;
    private String actualTitle;
    private String type;
    private String staticValue;
    private int sequence;

    public MasterKey () {
    }

    public MasterKey (String titleKey, String actualTitle, String type, String staticValue, int sequence) {
        this.titleKey = titleKey;
        this.actualTitle = actualTitle;
        this.type = type;
        this.staticValue = staticValue;
        this.sequence = sequence;
    }

    public Long getId () {
        return id;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public String getTitleKey () {
        return titleKey;
    }

    public void setTitleKey (String titleKey) {
        this.titleKey = titleKey;
    }

    public String getActualTitle () {
        return actualTitle;
    }

    public void setActualTitle (String actualTitle) {
        this.actualTitle = actualTitle;
    }

    public String getType () {
        return type;
    }

    public void setType (String type) {
        this.type = type;
    }

    public String getStaticValue () {
        return staticValue;
    }

    public void setStaticValue (String staticValue) {
        this.staticValue = staticValue;
    }

    public int getSequence () {
        return sequence;
    }

    public void setSequence (int sequence) {
        this.sequence = sequence;
    }
}
