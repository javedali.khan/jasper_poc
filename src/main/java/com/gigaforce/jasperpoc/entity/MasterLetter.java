package com.gigaforce.jasperpoc.entity;

import javax.persistence.*;

@Entity
public class MasterLetter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String letterType;
    @Lob
    private String masterBody;

    public MasterLetter () {
    }

    public MasterLetter (String letterType, String masterBody) {
        this.letterType = letterType;
        this.masterBody = masterBody;
    }

    public Long getId () {
        return id;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public String getLetterType () {
        return letterType;
    }

    public void setLetterType (String letterType) {
        this.letterType = letterType;
    }

    public String getMasterBody () {
        return masterBody;
    }

    public void setMasterBody (String masterBody) {
        this.masterBody = masterBody;
    }
}
