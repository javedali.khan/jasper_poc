package com.gigaforce.jasperpoc.model;


public class Address {

    private String firstAddress;
    private String secondAddress;
    private String phone;
    private String fax;

    public Address (String firstAddress, String secondAddress, String phone, String fax) {
        this.firstAddress = firstAddress;
        this.secondAddress = secondAddress;
        this.phone = phone;
        this.fax = fax;
    }

    public Address (String firstAddress) {
        this.firstAddress = firstAddress;
    }

    public String getFirstAddress () {
        return firstAddress;
    }

    public String getSecondAddress () {
        return secondAddress;
    }

    public String getPhone () {
        return phone;
    }

    public String getFax () {
        return fax;
    }
}
