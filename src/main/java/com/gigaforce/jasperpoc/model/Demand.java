package com.gigaforce.jasperpoc.model;

public class Demand {

    private String companyName;
    private String letterType;
    private Address address;
    private Subject subject;
    private EmailBody emailBody;
    private Address paymentAddress1;
    private Address paymentAddress2;
    private Signature signature;

    public Demand () {
    }

    public Demand (String companyName, String letterType, Address address, Subject subject, EmailBody emailBody,
            Address paymentAddress1, Address paymentAddress2, Signature signature) {
        this.companyName = companyName;
        this.letterType = letterType;
        this.address = address;
        this.subject = subject;
        this.emailBody = emailBody;
        this.paymentAddress1 = paymentAddress1;
        this.paymentAddress2 = paymentAddress2;
        this.signature = signature;
    }

    public String getCompanyName () {
        return companyName;
    }

    public void setCompanyName (String companyName) {
        this.companyName = companyName;
    }

    public String getLetterType () {
        return letterType;
    }

    public void setLetterType (String letterType) {
        this.letterType = letterType;
    }

    public Address getAddress () {
        return address;
    }

    public void setAddress (Address address) {
        this.address = address;
    }

    public Subject getSubject () {
        return subject;
    }

    public void setSubject (Subject subject) {
        this.subject = subject;
    }

    public EmailBody getEmailBody () {
        return emailBody;
    }

    public void setEmailBody (EmailBody emailBody) {
        this.emailBody = emailBody;
    }

    public Address getPaymentAddress1 () {
        return paymentAddress1;
    }

    public void setPaymentAddress1 (Address paymentAddress1) {
        this.paymentAddress1 = paymentAddress1;
    }

    public Address getPaymentAddress2 () {
        return paymentAddress2;
    }

    public void setPaymentAddress2 (Address paymentAddress2) {
        this.paymentAddress2 = paymentAddress2;
    }

    public Signature getSignature () {
        return signature;
    }

    public void setSignature (Signature signature) {
        this.signature = signature;
    }
}
