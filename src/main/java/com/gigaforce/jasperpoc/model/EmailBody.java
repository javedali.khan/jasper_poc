package com.gigaforce.jasperpoc.model;

public class EmailBody {
    private String body;

    public EmailBody (String body) {
        this.body = body;
    }

    public String getBody () {
        return body;
    }

    public void setBody (String body) {
        this.body = body;
    }
}
