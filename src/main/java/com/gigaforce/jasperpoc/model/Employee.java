package com.gigaforce.jasperpoc.model;

public class Employee {

    private String name;
    private String designation;
    private String note;

    public Employee () {
    }

    public Employee (String note) {
        this.note = note;
    }

    public Employee (String name, String designation) {
        this.name = name;
        this.designation = designation;
    }

    public String getName () {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public String getDesignation () {
        return designation;
    }

    public void setDesignation (String designation) {
        this.designation = designation;
    }

    public String getNote () {
        return note;
    }

    public void setNote (String note) {
        this.note = note;
    }
}