package com.gigaforce.jasperpoc.model;

public class Signature {
    private String signName;
    private String signPhone;
    private String signFax;
    private String signEmail;

    public Signature (String signName, String signPhone, String signFax, String signEmail) {
        this.signName = signName;
        this.signPhone = signPhone;
        this.signFax = signFax;
        this.signEmail = signEmail;
    }

    public Signature (String signName) {
        this.signName = signName;
    }

    public String getSignName () {
        return signName;
    }

    public void setSignName (String signName) {
        this.signName = signName;
    }

    public String getSignPhone () {
        return signPhone;
    }

    public void setSignPhone (String signPhone) {
        this.signPhone = signPhone;
    }

    public String getSignFax () {
        return signFax;
    }

    public void setSignFax (String signFax) {
        this.signFax = signFax;
    }

    public String getSignEmail () {
        return signEmail;
    }

    public void setSignEmail (String signEmail) {
        this.signEmail = signEmail;
    }
}
