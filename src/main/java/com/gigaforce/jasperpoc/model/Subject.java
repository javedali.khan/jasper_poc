package com.gigaforce.jasperpoc.model;

public class Subject {
    private String insured;
    private String dateOfLoss;
    private String nssNumber;
    private String insuranceNumber;
    private String underWritingCompany;
    private String lossLocation;
    private String amountOfClaim;
    private String amountOfPaidClaim;
    private String deductible;
    private String yourInsured;
    private String policyNumber;
    private String claimNumber;
    private String fileNumber;

    public Subject () {
    }

    public Subject (String insured, String dateOfLoss, String nssNumber, String insuranceNumber,
            String underWritingCompany, String lossLocation, String amountOfClaim, String amountOfPaidClaim,
            String deductible, String yourInsured, String policyNumber, String claimNumber, String fileNumber) {
        this.insured = insured;
        this.dateOfLoss = dateOfLoss;
        this.nssNumber = nssNumber;
        this.insuranceNumber = insuranceNumber;
        this.underWritingCompany = underWritingCompany;
        this.lossLocation = lossLocation;
        this.amountOfClaim = amountOfClaim;
        this.amountOfPaidClaim = amountOfPaidClaim;
        this.deductible = deductible;
        this.yourInsured = yourInsured;
        this.policyNumber = policyNumber;
        this.claimNumber = claimNumber;
        this.fileNumber = fileNumber;
    }

    public String getInsured () {
        return insured;
    }

    public void setInsured (String insured) {
        this.insured = insured;
    }

    public String getDateOfLoss () {
        return dateOfLoss;
    }

    public void setDateOfLoss (String dateOfLoss) {
        this.dateOfLoss = dateOfLoss;
    }

    public String getNssNumber () {
        return nssNumber;
    }

    public void setNssNumber (String nssNumber) {
        this.nssNumber = nssNumber;
    }

    public String getInsuranceNumber () {
        return insuranceNumber;
    }

    public void setInsuranceNumber (String insuranceNumber) {
        this.insuranceNumber = insuranceNumber;
    }

    public String getUnderWritingCompany () {
        return underWritingCompany;
    }

    public void setUnderWritingCompany (String underWritingCompany) {
        this.underWritingCompany = underWritingCompany;
    }

    public String getLossLocation () {
        return lossLocation;
    }

    public void setLossLocation (String lossLocation) {
        this.lossLocation = lossLocation;
    }

    public String getAmountOfClaim () {
        return amountOfClaim;
    }

    public void setAmountOfClaim (String amountOfClaim) {
        this.amountOfClaim = amountOfClaim;
    }

    public String getAmountOfPaidClaim () {
        return amountOfPaidClaim;
    }

    public void setAmountOfPaidClaim (String amountOfPaidClaim) {
        this.amountOfPaidClaim = amountOfPaidClaim;
    }

    public String getDeductible () {
        return deductible;
    }

    public void setDeductible (String deductible) {
        this.deductible = deductible;
    }

    public String getYourInsured () {
        return yourInsured;
    }

    public void setYourInsured (String yourInsured) {
        this.yourInsured = yourInsured;
    }

    public String getPolicyNumber () {
        return policyNumber;
    }

    public void setPolicyNumber (String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getClaimNumber () {
        return claimNumber;
    }

    public void setClaimNumber (String claimNumber) {
        this.claimNumber = claimNumber;
    }

    public String getFileNumber () {
        return fileNumber;
    }

    public void setFileNumber (String fileNumber) {
        this.fileNumber = fileNumber;
    }
}
