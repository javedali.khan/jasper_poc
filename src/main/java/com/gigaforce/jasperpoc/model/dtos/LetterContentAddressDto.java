package com.gigaforce.jasperpoc.model.dtos;

public class LetterContentAddressDto {
    private String firstAddress;
    private String secondAddress;
    private String phone;
    private String fax;

    public LetterContentAddressDto () {
    }

    public LetterContentAddressDto (String firstAddress, String secondAddress, String phone, String fax) {
        this.firstAddress = firstAddress;
        this.secondAddress = secondAddress;
        this.phone = phone;
        this.fax = fax;
    }

    public String getFirstAddress () {
        return firstAddress;
    }

    public void setFirstAddress (String firstAddress) {
        this.firstAddress = firstAddress;
    }

    public String getSecondAddress () {
        return secondAddress;
    }

    public void setSecondAddress (String secondAddress) {
        this.secondAddress = secondAddress;
    }

    public String getPhone () {
        return phone;
    }

    public void setPhone (String phone) {
        this.phone = phone;
    }

    public String getFax () {
        return fax;
    }

    public void setFax (String fax) {
        this.fax = fax;
    }
}
