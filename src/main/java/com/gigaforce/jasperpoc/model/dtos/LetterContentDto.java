package com.gigaforce.jasperpoc.model.dtos;

import java.util.ArrayList;
import java.util.List;


public class LetterContentDto {

    private String insured;
    private String dateOfLoss;
    private String nssNumber;
    private String insuranceNumber;
    private String underWritingCompany;
    private String lossLocation;
    private String amountOfClaim;
    private String amountOfPaidClaim;
    private String deductible;
    private String yourInsured;
    private String policyNumber;
    private String claimNumber;
    private String fileNumber;
    private String body;
    private String signName;
    private String signPhone;
    private String signFax;
    private String signEmail;
    private List<LetterContentAddressDto> letterContentAddressDtos = new ArrayList<>();

    public LetterContentDto () {
    }

    public LetterContentDto (String insured, String dateOfLoss, String nssNumber, String insuranceNumber,
            String underWritingCompany, String lossLocation, String amountOfClaim, String amountOfPaidClaim,
            String deductible, String yourInsured, String policyNumber, String claimNumber, String fileNumber,
            String body, String signName, String signPhone, String signFax, String signEmail,
            List<LetterContentAddressDto> letterContentAddressDtos) {
        this.insured = insured;
        this.dateOfLoss = dateOfLoss;
        this.nssNumber = nssNumber;
        this.insuranceNumber = insuranceNumber;
        this.underWritingCompany = underWritingCompany;
        this.lossLocation = lossLocation;
        this.amountOfClaim = amountOfClaim;
        this.amountOfPaidClaim = amountOfPaidClaim;
        this.deductible = deductible;
        this.yourInsured = yourInsured;
        this.policyNumber = policyNumber;
        this.claimNumber = claimNumber;
        this.fileNumber = fileNumber;
        this.body = body;
        this.signName = signName;
        this.signPhone = signPhone;
        this.signFax = signFax;
        this.signEmail = signEmail;
        this.letterContentAddressDtos = letterContentAddressDtos;
    }

    public String getInsured () {
        return insured;
    }

    public void setInsured (String insured) {
        this.insured = insured;
    }

    public String getDateOfLoss () {
        return dateOfLoss;
    }

    public void setDateOfLoss (String dateOfLoss) {
        this.dateOfLoss = dateOfLoss;
    }

    public String getNssNumber () {
        return nssNumber;
    }

    public void setNssNumber (String nssNumber) {
        this.nssNumber = nssNumber;
    }

    public String getInsuranceNumber () {
        return insuranceNumber;
    }

    public void setInsuranceNumber (String insuranceNumber) {
        this.insuranceNumber = insuranceNumber;
    }

    public String getUnderWritingCompany () {
        return underWritingCompany;
    }

    public void setUnderWritingCompany (String underWritingCompany) {
        this.underWritingCompany = underWritingCompany;
    }

    public String getLossLocation () {
        return lossLocation;
    }

    public void setLossLocation (String lossLocation) {
        this.lossLocation = lossLocation;
    }

    public String getAmountOfClaim () {
        return amountOfClaim;
    }

    public void setAmountOfClaim (String amountOfClaim) {
        this.amountOfClaim = amountOfClaim;
    }

    public String getAmountOfPaidClaim () {
        return amountOfPaidClaim;
    }

    public void setAmountOfPaidClaim (String amountOfPaidClaim) {
        this.amountOfPaidClaim = amountOfPaidClaim;
    }

    public String getDeductible () {
        return deductible;
    }

    public void setDeductible (String deductible) {
        this.deductible = deductible;
    }

    public String getYourInsured () {
        return yourInsured;
    }

    public void setYourInsured (String yourInsured) {
        this.yourInsured = yourInsured;
    }

    public String getPolicyNumber () {
        return policyNumber;
    }

    public void setPolicyNumber (String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getClaimNumber () {
        return claimNumber;
    }

    public void setClaimNumber (String claimNumber) {
        this.claimNumber = claimNumber;
    }

    public String getFileNumber () {
        return fileNumber;
    }

    public void setFileNumber (String fileNumber) {
        this.fileNumber = fileNumber;
    }

    public String getBody () {
        return body;
    }

    public void setBody (String body) {
        this.body = body;
    }

    public String getSignName () {
        return signName;
    }

    public void setSignName (String signName) {
        this.signName = signName;
    }

    public String getSignPhone () {
        return signPhone;
    }

    public void setSignPhone (String signPhone) {
        this.signPhone = signPhone;
    }

    public String getSignFax () {
        return signFax;
    }

    public void setSignFax (String signFax) {
        this.signFax = signFax;
    }

    public String getSignEmail () {
        return signEmail;
    }

    public void setSignEmail (String signEmail) {
        this.signEmail = signEmail;
    }

    public List<LetterContentAddressDto> getLetterContentAddressDtos () {
        return letterContentAddressDtos;
    }

    public void setLetterContentAddressDtos (List<LetterContentAddressDto> letterContentAddressDtos) {
        this.letterContentAddressDtos = letterContentAddressDtos;
    }
}
