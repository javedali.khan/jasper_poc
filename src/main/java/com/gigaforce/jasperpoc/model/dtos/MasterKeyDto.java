package com.gigaforce.jasperpoc.model.dtos;


public class MasterKeyDto {

    private String titleKey;
    private String actualTitle;
    private String type;
    private String staticValue;
    private int sequence;

    public MasterKeyDto () {
    }

    public MasterKeyDto (String titleKey, String actualTitle, String type, String staticValue, int sequence) {
        this.titleKey = titleKey;
        this.actualTitle = actualTitle;
        this.type = type;
        this.staticValue = staticValue;
        this.sequence = sequence;
    }

    public String getTitleKey () {
        return titleKey;
    }

    public void setTitleKey (String titleKey) {
        this.titleKey = titleKey;
    }

    public String getActualTitle () {
        return actualTitle;
    }

    public void setActualTitle (String actualTitle) {
        this.actualTitle = actualTitle;
    }

    public String getType () {
        return type;
    }

    public void setType (String type) {
        this.type = type;
    }

    public String getStaticValue () {
        return staticValue;
    }

    public void setStaticValue (String staticValue) {
        this.staticValue = staticValue;
    }

    public int getSequence () {
        return sequence;
    }

    public void setSequence (int sequence) {
        this.sequence = sequence;
    }
}
