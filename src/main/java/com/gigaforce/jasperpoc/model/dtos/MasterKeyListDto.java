package com.gigaforce.jasperpoc.model.dtos;

import java.util.ArrayList;
import java.util.List;

public class MasterKeyListDto {

    public List<MasterKeyDto> masterKeyDtoList = new ArrayList<>();
    public MasterKeyListDto () {
    }
    public MasterKeyListDto (List<MasterKeyDto> masterKeyDtoList) {
        this.masterKeyDtoList = masterKeyDtoList;
    }

    public List<MasterKeyDto> getMasterKeyDtoList () {
        return masterKeyDtoList;
    }

    public void setMasterKeyDtoList (List<MasterKeyDto> masterKeyDtoList) {
        this.masterKeyDtoList = masterKeyDtoList;
    }
}
