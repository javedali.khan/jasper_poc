package com.gigaforce.jasperpoc.model.dtos;

import com.gigaforce.jasperpoc.entity.LetterType;

public class RequestDto {
    private String claimUUID;
    private String companyName;
    private LetterType letterType;
    private LetterContentDto letterContentDto;

    public RequestDto () {
    }

    public RequestDto (String claimUUID, String companyName, LetterContentDto letterContentDto) {
        this.claimUUID = claimUUID;
        this.companyName = companyName;
        this.letterContentDto = letterContentDto;
    }

    public String getClaimUUID () {
        return claimUUID;
    }

    public void setClaimUUID (String claimUUID) {
        this.claimUUID = claimUUID;
    }

    public String getCompanyName () {
        return companyName;
    }

    public void setCompanyName (String companyName) {
        this.companyName = companyName;
    }

    public LetterContentDto getLetterContentDto () {
        return letterContentDto;
    }

    public void setLetterContentDto (LetterContentDto letterContentDto) {
        this.letterContentDto = letterContentDto;
    }
}
