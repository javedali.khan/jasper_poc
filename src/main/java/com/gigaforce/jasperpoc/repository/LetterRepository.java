package com.gigaforce.jasperpoc.repository;

import com.gigaforce.jasperpoc.entity.Letter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LetterRepository extends JpaRepository<Letter,Long> {
}
