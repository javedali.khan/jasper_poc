package com.gigaforce.jasperpoc.repository;

import com.gigaforce.jasperpoc.entity.MasterKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MasterKeyRepository extends JpaRepository<MasterKey,Long> {

    List<MasterKey> findByTitleKeyIn(List<String> keys);
}
