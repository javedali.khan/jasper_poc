package com.gigaforce.jasperpoc.repository;

import com.gigaforce.jasperpoc.entity.MasterLetter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface MasterLetterRepository extends JpaRepository<MasterLetter, Long> {

    MasterLetter findByLetterType(String type);
}