package com.gigaforce.jasperpoc.service;

import com.gigaforce.jasperpoc.entity.Letter;
import com.gigaforce.jasperpoc.model.Demand;
import org.springframework.http.ResponseEntity;


public interface DemandService {

    Demand getDemand();

    Demand getEmail();

    Letter getLetter(Long id);
}
