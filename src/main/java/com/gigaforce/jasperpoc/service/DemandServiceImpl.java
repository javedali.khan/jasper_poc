package com.gigaforce.jasperpoc.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gigaforce.jasperpoc.entity.Letter;
import com.gigaforce.jasperpoc.model.*;
import com.gigaforce.jasperpoc.repository.LetterRepository;
import com.github.javafaker.Faker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class DemandServiceImpl implements DemandService {

    String msg = "State Auto has paid their insured Donald Parsley for damages sustained in an accident on 07/29/2021. National" +
            "Subrogation Services, as recovery agent for State Auto has investigated the facts and determined the accident" +
            "was due to your insured’s negligence.\n" +
            "Due to the payment to Donald Parsley, we are pursuing the subrogation rights of State Auto. The amount we" +
            "are claiming is the amount paid plus the deductible as noted above. We have enclosed documentation to" +
            "substantiate our claim." ;

    final Faker faker = new Faker();
    final Random random = new Random();

    @Autowired
    private LetterRepository letterRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public Demand getDemand () {
        Demand demand = new Demand();

        demand.setCompanyName("Gigaforce");
        demand.setAddress(findAddress());
        demand.setSubject(getSubject());
        demand.setEmailBody(getEmailBody());
        //demand.setPaymentAddress1(findAddress());
        demand.setPaymentAddress2(findAddress());
        demand.setSignature(getSign());

        return demand;
    }

    @Override
    public Demand getEmail () {
        Demand demand = new Demand();

        /*Subject subject = getSubject();
        subject.setUnderWritingCompany(null);

        demand.setCompanyName("Gigaforce");
        demand.setAddress(findAddress());
        demand.setSubject(subject);
        demand.setEmailBody(getEmailBody());
        demand.setSignature(getSign());*/
        Long id =1L;
        Letter letter = letterRepository.findById(id).get();

        demand.setCompanyName(letter.getCompanyName());
       // demand.setAddress(objectMapper.readValue());
        //demand.setSubject(subject);
        demand.setEmailBody(getEmailBody());
        demand.setSignature(getSign());
        return demand;
    }

    @Override
    public Letter getLetter (Long id) {
        return letterRepository.findById(id).get();
    }

    public Subject getSubject () {
        return new Subject(faker.superhero().name(), "07/22/2023",
                           faker.number().digits(10), faker.number().digits(15), faker.company().name(),
                           faker.address().cityName(),faker.number().digits(5),faker.number().digits(5),
                           faker.number().digits(3),faker.superhero().name(),faker.number().digits(7),
                           faker.number().digits(5),faker.number().digits(7));
    }

    public Address findAddress(){
        return new Address(faker.address().fullAddress(), faker.address().secondaryAddress(),
                           faker.phoneNumber().phoneNumber(), faker.phoneNumber().extension());
    }

    public EmailBody getEmailBody () {
        return new EmailBody(msg);
    }

    public Signature getSign () {
        return new Signature(faker.name().fullName(), faker.phoneNumber().phoneNumber(),
                                            faker.phoneNumber().extension(),faker.internet().emailAddress() );
    }
}
