package com.gigaforce.jasperpoc.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.gigaforce.jasperpoc.entity.MasterLetter;
import com.gigaforce.jasperpoc.model.*;
import com.gigaforce.jasperpoc.model.dtos.MasterKeyListDto;

import java.util.List;

public interface EmployeeService {
    List<Employee> findAll(JsonNode jsonNode) throws JsonProcessingException;

    List<Address> findAddress(JsonNode jsonNode) throws JsonProcessingException;

    Subject getSubject();

    List<EmailBody> getEmailBody();

    List<Signature> getSign(JsonNode jsonNode) throws JsonProcessingException;

    List<EmailBody> getBodyFromJson(JsonNode jsonNode) throws JsonProcessingException;

    String getCompanyName(JsonNode jsonNode);

    void saveMasterData(MasterKeyListDto masterKeyListDto);
    void saveMasterLetter(MasterLetter masterLetter);
}
