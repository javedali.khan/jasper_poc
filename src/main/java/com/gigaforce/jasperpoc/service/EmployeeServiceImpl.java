package com.gigaforce.jasperpoc.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.gigaforce.jasperpoc.entity.MasterKey;
import com.gigaforce.jasperpoc.entity.MasterLetter;
import com.gigaforce.jasperpoc.repository.MasterLetterRepository;
import com.gigaforce.jasperpoc.model.*;
import com.gigaforce.jasperpoc.model.dtos.MasterKeyDto;
import com.gigaforce.jasperpoc.model.dtos.MasterKeyListDto;
import com.gigaforce.jasperpoc.repository.LetterRepository;
import com.gigaforce.jasperpoc.repository.MasterKeyRepository;
import com.github.javafaker.Faker;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class EmployeeServiceImpl implements EmployeeService{

    String msg = "Nationwide has engaged National Subrogation Services, LLC (NSS) to handle the recovery portion of your" +
            "claim, should a payment be made.\n" +
            "The NSS Subrogation unit will make reasonable efforts to collect your deductible interest (if applicable) as" +
            "well as Nationwide’s financial interest. We appreciate your cooperation as NSS pursues this claim.\n" +
            "It is not necessary for you to take any action at this time. You can contact NSS by mail at 100 Crossways" +
            "Park West, Suite 415, Woodbury, NY 11797. The office toll free phone number is 800-850-5165, ext." +
            "18027 or email: TPietruszynski@nationalsubrogation.com.\n\n" +
            "Please contact the undersigned with any questions.\n\n" + "Sincerely,";

    final Faker faker = new Faker();
    final Random random = new Random();

    @Autowired
    private LetterRepository letterRepository;

    @Autowired
    private ReportService reportService;
    @Autowired
    private MasterKeyRepository masterKeyRepository;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private MasterLetterRepository masterLetterRepository;

    @Override
    public List<Employee> findAll (JsonNode jsonNode) throws JsonProcessingException {
        final List<Employee> subject = new ArrayList<>();

        List<MasterKey> masterKey = reportService.readJson(jsonNode);

        masterKey.stream()
                .filter(masterKey1 -> Objects.equals(masterKey1.getType(), "subject"))
                .sorted(Comparator.comparing(MasterKey::getSequence))
                .forEachOrdered(masterKey1 -> {
                    String prefix = !StringUtils.isBlank(masterKey1.getStaticValue()) ? masterKey1.getStaticValue()+" " : "";

                    String value = !StringUtils.isBlank(masterKey1.getStaticValue()) ?
                            StringUtils.replaceIgnoreCase(masterKey1.getStaticValue(), '#'+masterKey1.getTitleKey()+'#', getValueByKey(jsonNode, masterKey1.getTitleKey()))
                            : getValueByKey(jsonNode, masterKey1.getTitleKey());

                    //old/////subject.add(new Employee(masterKey1.getValue(), prefix + getValueByKey(jsonNode, masterKey1.getKey())));
                    subject.add(new Employee(masterKey1.getActualTitle(), value));
                });
        return subject;


    }

    @Override
    public List<Address> findAddress(JsonNode jsonNode) throws JsonProcessingException {
        List<Address> addressList = new ArrayList<>();
        /*List<MasterKey> masterKey = reportService.readJson(jsonNode);
        masterKey.stream().filter(masterKey1 -> Objects.equals(masterKey1.getType(), "address"))
                .forEachOrdered(masterKey1 ->
                                 addressList.add(new Address(getValueByKey(jsonNode,masterKey1.getKey()))));*/

       Address address = new Address(getValueByKey(jsonNode,"address"), getValueByKey(jsonNode,"city"),
                                     getValueByKey(jsonNode,"phone"), getValueByKey(jsonNode,"fax"));
        addressList.add(address);
        return addressList;
    }

    @Override
    public Subject getSubject () {
        return new Subject(faker.superhero().name(), "07/22/2023",
                           faker.number().digits(10), faker.number().digits(15), faker.company().name(),
                           faker.address().cityName(),faker.number().digits(5),faker.number().digits(5),
                           faker.number().digits(3),faker.superhero().name(),faker.number().digits(7),
                           faker.number().digits(5),faker.number().digits(7));
    }

    @Override
    public List<EmailBody> getEmailBody () {

        List<EmailBody> bodyList = new ArrayList<>();
        EmailBody emailBody = new EmailBody(msg);
        bodyList.add(emailBody);
        return bodyList;

    }

    /*@Override
    public List<Signature> getSign (JsonNode jsonNode) {
        List<Signature> signatureList = new ArrayList<>();
        Signature signature = new Signature(getValueByKey(jsonNode,"signName"),getValueByKey(jsonNode,"signPhone"),
                                            getValueByKey(jsonNode,"signFax"),getValueByKey(jsonNode,"signEmail"));
        signatureList.add(signature);
        return signatureList;
    }*/

    @Override
    public List<Signature> getSign (JsonNode jsonNode) throws JsonProcessingException {
        List<Signature> signatureList = new ArrayList<>();
        List<MasterKey> masterKey = reportService.readJson(jsonNode);
        masterKey.stream()
                .filter(masterKey1 -> Objects.equals(masterKey1.getType(), "signature"))
                .sorted(Comparator.comparing(MasterKey::getSequence))
                .forEachOrdered(masterKey1 ->
                                {
                                    String prefix = !StringUtils.isBlank(masterKey1.getActualTitle()) ? masterKey1.getActualTitle() : "";
                                    signatureList.add(new Signature(prefix+getValueByKey(jsonNode,masterKey1.getTitleKey())));
                                });
        return signatureList;
    }

    @Override
    @Transactional
    public List<EmailBody> getBodyFromJson (JsonNode jsonNode) throws JsonProcessingException {
        List<EmailBody> bodyList = new ArrayList<>();
        String body;

        body = getValueByKey(jsonNode, "body");

        boolean db = false;

        if(db){
            String letter_type = getValueByKey(jsonNode, "letterType");
             body = masterLetterRepository.findByLetterType(letter_type).getMasterBody();
        }

        List<MasterKey> masterKey = masterKeyRepository.findAll();

        AtomicReference<String> finalBody = new AtomicReference<>(body);
        String finalBody1 = body;
        masterKey.stream().filter(masterKey1 -> Objects.equals(masterKey1.getType(), "body"))
                .forEach(masterKey1 -> {
                                    String[] key = masterKey1.getTitleKey().split("_");
                                    String valueByKey = getValueByKey(jsonNode, key[0]);
                                    if (finalBody1 != null && finalBody1.contains(masterKey1.getTitleKey())) {
                                        finalBody.set(
                                                StringUtils.replaceIgnoreCase(String.valueOf(finalBody), masterKey1.getTitleKey(), valueByKey));
                                    }
                                }
                );

        //String replace = getValueByKey(jsonNode,"dateOfLoss");
        //body = StringUtils.replaceIgnoreCase(body, "dateOfLoss", replace);

        bodyList.add(new EmailBody(finalBody.get()));
        return bodyList;
    }

    @Override
    public String getCompanyName (JsonNode jsonNode) {
        return getValueByKey(jsonNode,"companyName");
    }

    @Override
    public void saveMasterData (MasterKeyListDto masterKeyListDto) {

        List<MasterKey> masterKeyList = new ArrayList<>();

        MasterKey map = null;
        for (MasterKeyDto masterKeyDto : masterKeyListDto.getMasterKeyDtoList()) {
            map = modelMapper.map(masterKeyDto, MasterKey.class);
            masterKeyList.add(map);
        }

        masterKeyRepository.saveAll(masterKeyList);

    }

    @Override
    public void saveMasterLetter (MasterLetter masterLetter) {
        masterLetterRepository.save(masterLetter);
    }

    public static String getValueByKey(JsonNode jsonNode, String key) {
        if (jsonNode.has(key)) {
            JsonNode valueNode = jsonNode.get(key);
            if (valueNode.isValueNode()) {
                return valueNode.asText();
            } /*else if (valueNode.isObject()) {
                return getValueByKey(valueNode, key); // Recursive call for nested object
            }*/
        }

        // Traverse through the child nodes
        if (jsonNode.isObject()) {
            for (JsonNode childNode : jsonNode) {
                String value = getValueByKey(childNode, key);
                if (value != null) {
                    return value;
                }
            }
        }if (jsonNode.isArray()) {
            for (JsonNode childNode : jsonNode) {
                String value = getValueByKey(childNode, key);
                if (value != null) {
                    return value;
                }
            }
        }

        return null;
    }
}
