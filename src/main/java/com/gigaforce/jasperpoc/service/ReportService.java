package com.gigaforce.jasperpoc.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.gigaforce.jasperpoc.entity.MasterKey;
import com.gigaforce.jasperpoc.model.Demand;
import com.gigaforce.jasperpoc.model.dtos.RequestDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ReportService {
    ResponseEntity<byte[]> getReport(Long id);
    Demand getLetterContent(Long id) throws JsonProcessingException;

    RequestDto saveLetter(RequestDto requestDto) throws JsonProcessingException;

    List<MasterKey> readJson(JsonNode json) throws JsonProcessingException;
}
