package com.gigaforce.jasperpoc.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.gigaforce.jasperpoc.entity.Letter;
import com.gigaforce.jasperpoc.entity.LetterContentAddress;
import com.gigaforce.jasperpoc.entity.MasterKey;
import com.gigaforce.jasperpoc.model.*;
import com.gigaforce.jasperpoc.model.dtos.RequestDto;
import com.gigaforce.jasperpoc.repository.LetterRepository;
import com.gigaforce.jasperpoc.repository.MasterKeyRepository;
import net.sf.jasperreports.engine.*;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.*;

@Service
public class ReportServiceImpl implements ReportService {

    final Logger log = LoggerFactory.getLogger(this.getClass());
    private final String template_path = "/demand.jrxml";
    private static final String gigaforce_logo_path = "/logo.png";
    private static final String nss_logo_path = "/nss.png";
    @Autowired
    private LetterRepository letterRepository;
    @Autowired
    private MasterKeyRepository masterKeyRepository;
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public ResponseEntity<byte[]> getReport (Long id) {
        log.info("inside report method");

        try {
            Demand demand  = getLetterContent(id);

            // Adding the additional parameters to the pdf.
            final Map<String, Object> parameters = new HashMap<>();
            parameters.put("demand", demand);
            parameters.put("company", demand.getCompanyName());
            parameters.put("logo", getLogo(demand.getCompanyName()));

            return jasperHandler(parameters);

        } catch (JRException | JsonProcessingException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public InputStream getLogo(String companyName){
        InputStream resourceAsStream = null;
        if(companyName.equalsIgnoreCase("Gigaforce")){
            resourceAsStream = getClass().getResourceAsStream(gigaforce_logo_path);
        } else if (companyName.equalsIgnoreCase("National Subrogation Service")) {
            resourceAsStream = getClass().getResourceAsStream(nss_logo_path);
        }else {
            resourceAsStream = getClass().getResourceAsStream(gigaforce_logo_path);
        }
        return resourceAsStream;
    }

    public Demand getLetterContent (Long id) throws JsonProcessingException {

        Letter letter = letterRepository.findById(id).get();

        Address address = Objects.nonNull(letter.getLetterContent()) ? objectMapper.readValue(
                objectMapper.writeValueAsString(letter.getLetterContent().getLetterContentAddresses().get(0)), new TypeReference<Address>() {
                }) : null;

        Address paymentAddress = null;
        Address paymentAddress2 = null;
        if (Objects.nonNull(letter.getLetterContent().getLetterContentAddresses()) &&
                letter.getLetterContent().getLetterContentAddresses().size() > 1)
        {
            paymentAddress = Objects.nonNull(letter.getLetterContent()) ? objectMapper.readValue(
                    objectMapper.writeValueAsString(letter.getLetterContent().getLetterContentAddresses().get(1)),
                    new TypeReference<Address>() {
                    }) : null;

            paymentAddress2 = Objects.nonNull(letter.getLetterContent()) ? objectMapper.readValue(
                    objectMapper.writeValueAsString(letter.getLetterContent().getLetterContentAddresses().get(2)),
                    new TypeReference<Address>() {
                    }) : null;
        }

        EmailBody emailBody = Objects.nonNull(letter.getLetterContent()) ? objectMapper.readValue(
                objectMapper.writeValueAsString(letter.getLetterContent().getBody()), new TypeReference<EmailBody>() {
                }) : null;

        Subject subject = Objects.nonNull(letter.getLetterContent()) ? objectMapper.readValue(
                objectMapper.writeValueAsString(letter.getLetterContent()), new TypeReference<Subject>() {
                }) : null;

        Signature signature = Objects.nonNull(letter.getLetterContent()) ? objectMapper.readValue(
                objectMapper.writeValueAsString(letter.getLetterContent()), new TypeReference<Signature>() {
                }) : null;

        Demand demand = new Demand();

        demand.setCompanyName(letter.getCompanyName());
        demand.setLetterType(letter.getLetterType().name());
        demand.setAddress(address);
        demand.setSubject(subject);
        demand.setEmailBody(emailBody);
        demand.setPaymentAddress1(paymentAddress);
        demand.setPaymentAddress2(paymentAddress2);
        demand.setSignature(signature);

        return demand;
    }

    @Override
    public RequestDto saveLetter (RequestDto requestDto) throws JsonProcessingException {


        Letter letter = modelMapper.map(requestDto,Letter.class);

        List<LetterContentAddress> letterContentAddressList = new ArrayList<>();

       requestDto.getLetterContentDto().getLetterContentAddressDtos().forEach(
               data-> letterContentAddressList.add(modelMapper.map(data, LetterContentAddress.class)));

        letter.getLetterContent().setLetterContentAddresses(letterContentAddressList);

        letterRepository.save(letter);
         return requestDto;
    }

    @Override
    public List<MasterKey> readJson (JsonNode json) throws JsonProcessingException {
        List<String> keys = new ArrayList<>();
        getAllKeysUsingJsonNodeFieldNames(json, keys);
        return masterKeyRepository.findByTitleKeyIn(keys);
    }

    private void getAllKeysUsingJsonNodeFieldNames(JsonNode json, List<String> keys) {

        if (json.isObject()) {
            Iterator<Map.Entry<String, JsonNode>> fields = json.fields();
            fields.forEachRemaining(field -> {
                keys.add(field.getKey());
                getAllKeysUsingJsonNodeFieldNames(field.getValue(), keys);
            });
        } else if (json.isArray()) {
            ArrayNode arrayField = (ArrayNode) json;
            arrayField.forEach(node -> getAllKeysUsingJsonNodeFieldNames(node, keys));
        }
    }

    private ResponseEntity<byte[]> jasperHandler (Map<String, Object> parameters) throws JRException {
        // Fetching the .jrxml file from the resource folder.
        final InputStream stream = this.getClass().getResourceAsStream(template_path);

        // Compile the Jasper report from .jrxml to .japser
        final JasperReport report = JasperCompileManager.compileReport(stream);

        // Fetching the employees from the data source.
        //JRBeanCollectionDataSource signDataBean = new JRBeanCollectionDataSource(employeeService.getDemand());

        // Filling the report with the employee data and additional parameters information.
        final JasperPrint print = JasperFillManager.fillReport(report, parameters, new JREmptyDataSource());
        //new JREmptyDataSource()

        HttpHeaders headers = new HttpHeaders();
        //set the PDF format
        headers.setContentType(MediaType.APPLICATION_PDF);
        headers.setContentDispositionFormData("filename", "demand.pdf");
        //create the report in PDF format
        return new ResponseEntity<>(JasperExportManager.exportReportToPdf(print), headers, HttpStatus.OK);
    }
}
