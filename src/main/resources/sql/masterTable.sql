INSERT INTO public.master_key
(id, "key", value)
VALUES(1, 'insured', 'Insured');
INSERT INTO public.master_key
(id, "key", value)
VALUES(2, 'dateOfLoss', 'Date of Loss');
INSERT INTO public.master_key
(id, "key", value)
VALUES(3, 'insuranceNumber', 'Insurance Co Claim #');
INSERT INTO public.master_key
(id, "key", value)
VALUES(4, 'nssNumber', 'NSS Matter Number');
INSERT INTO public.master_key
(id, "key", value)
VALUES(5, 'underWritingCompany', 'Under Writing Company');
INSERT INTO public.master_key
(id, "key", value)
VALUES(6, 'lossLocation', 'Loss Location');
INSERT INTO public.master_key
(id, "key", value)
VALUES(7, 'amountOfClaim', 'Amount of Claim');
INSERT INTO public.master_key
(id, "key", value)
VALUES(8, 'amountOfPaidClaim', 'Amount of Paid Claim');
INSERT INTO public.master_key
(id, "key", value)
VALUES(9, 'deductible', 'Deductible');
INSERT INTO public.master_key
(id, "key", value)
VALUES(10, 'yourInsured', 'Your Insured');
INSERT INTO public.master_key
(id, "key", value)
VALUES(11, 'policyNumber', 'Your Policy Number');
INSERT INTO public.master_key
(id, "key", value)
VALUES(12, 'claimNumber', 'Your Claim Number:');
INSERT INTO public.master_key
(id, "key", value)
VALUES(13, 'fileNumber', 'File Number');
